// this is: ./StandAlone/src/main.cpp

#include <QTextStream>
#include <QTranslator>

int main(int argc, char **argv, char **env)
{
    QTranslator translator;

    if (translator.load(QLocale::system().name(), ":/cmake-issue-23611/share/ts"))
        QCoreApplication::installTranslator(&translator);

	QTextStream(stdout) << QString::tr("Thanks to all these cool people that helped contributing to cmake-issue-23611 all these years.");
	
	
	return 0;
}
/*      <source>Thanks to all these cool people that helped contributing to Webcamoid all these years.</source>
        <translation>Vielen Dank an all die coolen Leute, die all die Jahre dazu beigetragen haben, Webcamoid zu unterstützen.</translation>
*/
